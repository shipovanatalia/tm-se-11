package ru.shipova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.Session;
import ru.shipova.tm.endpoint.SessionEndpoint;
import ru.shipova.tm.service.SessionService;

public final class UserLogoutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign out from Task Manager.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final SessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpoint();
            if (sessionEndpoint == null) return;
            @NotNull final SessionService sessionService = serviceLocator.getSessionService();
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            try {
                if (adminUserEndpoint == null) return;
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                sessionEndpoint.closeSession(session);
                sessionService.setSession(null);
                System.out.println("[OK]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
