package ru.shipova.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.repository.ISessionRepository;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.util.SignatureUtil;

import java.util.Date;
import java.util.UUID;

@RequiredArgsConstructor
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {
    @NotNull
    private final IServiceLocator serviceLocator;

    @Nullable
    public Session open(@NotNull final String login, @NotNull final String password) {
        @Nullable final User user = serviceLocator.getIUserService().authorize(login, password);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setId(UUID.randomUUID().toString());
        session.setUserId(user.getId());
        session.setTimestamp(new Date().getTime());
        @NotNull final String salt = "SomeVerySpicySalt";
        @Nullable final String signature = SignatureUtil.sign(session, salt, 100);
        if (signature == null || signature.isEmpty()) return null;
        session.setSignature(signature);
        getMap().put(session.getId(), session);
        return session;
    }

    @Nullable
    public Result close(@NotNull final Session session) {
        remove(session);
        return new Result();
    }

    @Override
    public boolean contains(@NotNull String sessionId) {
        @Nullable final Session session = findOne(sessionId);
        return session != null;
    }
}
