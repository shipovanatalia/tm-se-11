package ru.shipova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;

import java.util.List;

public interface IUserService {

    void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role);

    void updateUser(@Nullable final String login, @Nullable final String role);

    RoleType getRoleType(@NotNull final Session session);

    RoleType getRoleType(@Nullable final String role);

    void setNewPassword(@Nullable final String userLogin, @NotNull final String password);

    User authorize(@NotNull final String login, @NotNull final String password);

    boolean checkDataAccess(@NotNull final String login, @NotNull final String password);

    User findByLogin(@Nullable final String login);

    User findById(@NotNull final String userId);

    void setCurrentUser(@Nullable final User currentUser);

    User getCurrentUser();

    @Nullable List<User> getUserList();

    void load(@Nullable final List<User> userList);
}
