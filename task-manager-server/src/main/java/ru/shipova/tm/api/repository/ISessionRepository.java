package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;

public interface ISessionRepository {
    @Nullable
    Session open(@NotNull final String login, @NotNull final String password);
    @Nullable
    Result close(@NotNull final Session session);
    boolean contains(@NotNull final String sessionId);
}
