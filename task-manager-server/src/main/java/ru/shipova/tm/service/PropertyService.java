package ru.shipova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public class PropertyService {
    @NotNull private final String serverHost = "localhost";
    @NotNull private final Integer serverPort = 8180;
}
