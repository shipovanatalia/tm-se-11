package ru.shipova.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.repository.ISessionRepository;
import ru.shipova.tm.api.service.ISessionService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.SignatureUtil;

@RequiredArgsConstructor
public class SessionService implements ISessionService {
    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IServiceLocator serviceLocator;

    @Override
    public @Nullable Session open(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return sessionRepository.open(login, password);
    }

    @Override
    public @Nullable Result close(@NotNull final Session session) {
        return sessionRepository.close(session);
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException {
        if (session == null) throw new AccessForbiddenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        SignatureUtil.sign(temp, "SomeVerySpicySalt", 100);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        if (!sessionRepository.contains(session.getId())) throw new AccessForbiddenException();
    }
}
