package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.service.IDomainService;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.entity.*;
import ru.shipova.tm.serializer.*;

import java.io.IOException;
import java.util.List;

public class DomainService implements IDomainService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void deserialize(@NotNull final Session session, @NotNull final String serializer) {
        if (Serializer.BINARY.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            @Nullable Domain domain;
            try {
                domain = binarySerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Serializer.JSON_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
            @Nullable Domain domain;
            try {
                domain = jsonFasterXmlSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Serializer.JSON_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
            @Nullable Domain domain;
            try {
                domain = jsonJaxBSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Serializer.XML_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
            @Nullable Domain domain;
            try {
                domain = xmlFasterXmlSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (Serializer.XML_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
            @Nullable Domain domain;
            try {
                domain = xmlJaxBSerializer.deserialize();
                load(domain);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getIUserService().load(domain.getUserList());
        serviceLocator.getIProjectService().load(domain.getProjectList());
        serviceLocator.getITaskService().load(domain.getTaskList());
    }

    @Override
    public void serialize(@NotNull final Session session, @NotNull final Domain domain, @NotNull String serializer) throws IOException {
        domain.setUserId(session.getUserId());
        export(domain);
        if (Serializer.BINARY.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            binarySerializer.serialize(domain);
        }
        if (Serializer.JSON_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
            jsonFasterXmlSerializer.serialize(domain);
        }
        if (Serializer.JSON_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
            jsonJaxBSerializer.serialize(domain);
        }
        if (Serializer.XML_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
            xmlFasterXmlSerializer.serialize(domain);
        }
        if (Serializer.XML_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
            xmlJaxBSerializer.serialize(domain);
        }
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        @NotNull final String userId = domain.getUserId();
        @Nullable final List<User> userList = serviceLocator.getIUserService().getUserList();
        if (userList == null) return;
        @Nullable final List<Project> projectList = serviceLocator.getIProjectService().getProjectListOfUser(userId);
        if (projectList == null) return;
        @Nullable final List<Task> taskList = serviceLocator.getITaskService().getTaskListOfUser(userId);
        if (taskList == null) return;
        domain.setUserList(userList);
        domain.setProjectList(projectList);
        domain.setTaskList(taskList);
    }
}
