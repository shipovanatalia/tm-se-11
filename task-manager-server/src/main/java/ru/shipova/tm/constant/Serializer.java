package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;

public enum Serializer {
    BINARY("BINARY"),
    JSON_FASTER_XML("JSON_FASTER_XML"),
    JSON_JAX_B("JSON_JAX_B"),
    XML_FASTER_XML("XML_FASTER_XML"),
    XML_JAX_B("XML_JAX_B");

    private @NotNull
    final String name;

    Serializer(@NotNull final String name) {
        this.name = name;
    }

    @NotNull public String displayName() {
        return name;
    }
}
