package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name = "AdminUserEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class AdminUserEndpoint extends AbstractEndpoint{
    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public boolean haveAccessToUsualCommand(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "needAuthorize", partName = "needAuthorize") final boolean needAuthorize
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return false;
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getIAdminUserService().getAccessToUsualCommand(session, needAuthorize);
    }

    @WebMethod
    public boolean haveAccessToAdminCommand(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "isOnlyAdminCommand", partName = "isOnlyAdminCommand") final boolean isOnlyAdminCommand
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return false;
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getIAdminUserService().getAccessToAdminCommand(session, isOnlyAdminCommand);
    }
}
