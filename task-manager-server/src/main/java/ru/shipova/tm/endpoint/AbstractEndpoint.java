package ru.shipova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
@NoArgsConstructor
public class AbstractEndpoint {
    @Nullable
    public IServiceLocator serviceLocator;

    public AbstractEndpoint(@Nullable final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
