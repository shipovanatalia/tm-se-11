package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.exception.ProjectDoesNotExistException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name = "TaskEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class TaskEndpoint extends AbstractEndpoint{

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @NotNull
    public List<Task> showAllTasksOfProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "projectName", partName = "projectName") @NotNull final String projectName
    ) throws AccessForbiddenException, ProjectDoesNotExistException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getITaskService().showAllTasksOfProject(projectName);
    }

    @WebMethod
    @Nullable
    public List<Task> getTaskListOfUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getITaskService().getTaskListOfUser(session.getUserId());
    }

    public List<Task> getSortedTaskListOfUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "typeOfSort", partName = "typeOfSort") @NotNull final String typeOfSort
    ) throws AccessForbiddenException, CommandCorruptException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        List<Task> sortedList = serviceLocator.getITaskService().getSortedTaskListOfUser(session.getUserId(), typeOfSort);
        if (sortedList == null) return new ArrayList<>();
        return sortedList;
    }

    @WebMethod
    @Nullable
    public List<Task> searchTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "partOfData", partName = "partOfData") final String partOfData
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getITaskService().search(session.getUserId(), partOfData);
    }

    @WebMethod
    public void createTask(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    ) throws AccessForbiddenException, ProjectDoesNotExistException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().create(session.getUserId(), taskName, projectName);
    }

    @WebMethod
    public void clearTaskListOfUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().clear(session.getUserId());
    }

    @WebMethod
    public void removeTaskFromUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().remove(session.getUserId(), taskName);
    }

    @WebMethod
    public void loadTaskList(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "taskList", partName = "taskList") @Nullable final List<Task> taskList
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getITaskService().load(taskList);
    }
}
