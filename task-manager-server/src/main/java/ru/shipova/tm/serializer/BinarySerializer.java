package ru.shipova.tm.serializer;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.constant.DataConstant;
import ru.shipova.tm.entity.Domain;

import java.io.*;

public class BinarySerializer implements ISerializer {
    @Override
    public void serialize(@NotNull final Domain domain) throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
        try (@NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
             @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(domain);
        }
    }

    @Override
    @Nullable
    public Domain deserialize(){
        @NotNull final File file = new File(DataConstant.FILE_BINARY.displayName());
        @Nullable Domain domain = null;
        try(@NotNull final FileInputStream fileInputStream = new FileInputStream(file);
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
           domain = (Domain) objectInputStream.readObject();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return domain;
    }
}
